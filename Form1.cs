﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Runtime.InteropServices;
using VersionControl.NET;

namespace GILSTesterFFTestStationSample
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            timer_DelayBeforeAutomaticMachineVersionCheck = new System.Windows.Forms.Timer();
            timer_DelayBeforeAutomaticMachineVersionCheck.Tick += new EventHandler(timer_DelayBeforeAutomaticMachineVersionCheck_tick);

        }

        private void timer_DelayBeforeAutomaticMachineVersionCheck_tick(Object myObject, EventArgs myEventArgs)
        {
            //MessageBox.Show("Inside timer_DelayBeforeAutomaticMachineVersionCheck_tick");

            timer_DelayBeforeAutomaticMachineVersionCheck.Stop();
            timer_DelayBeforeAutomaticMachineVersionCheck.Enabled = false;

            //string machineId = Dll.GetDllKeyValue("CheckProgramVersion", "machineId");

            //bool machineVersionCheckPass =
            //Dll.FailedVersionCheckInfo[] failedVersionCheckInfos =
            Dll.MachineOpenLotVersionCheckInfo machineOpenLotFailedVersionCheckInfo =
                Dll.CheckMachineVersion(
                    //machineId,//comboBoxTestMachineId.Text,
                    comboBoxLotId.Text,
                    comboBoxToken.Text);// token);

            //if (machineVersionCheckPass)
            //if (failedVersionCheckInfos == null)
            if (machineOpenLotFailedVersionCheckInfo.machineProceedWithOpenLot)
                MessageBox.Show("Machine version check PASS");
            else
            {
                string failureDetails = "";

                for (int i = 0; i < machineOpenLotFailedVersionCheckInfo.programFailedVersionCheckInfos.Length; i++)
                {
                    failureDetails +=
                        machineOpenLotFailedVersionCheckInfo.programFailedVersionCheckInfos[i].ipAddress
                        + ": " + machineOpenLotFailedVersionCheckInfo.programFailedVersionCheckInfos[i].programType
                        + ": " + machineOpenLotFailedVersionCheckInfo.programFailedVersionCheckInfos[i].failureInfo
                        + "\n\n";
                }

                MessageBox.Show(string.Format(
                    "Machine version check FAILED:\n\n{0}",
                    failureDetails));
            }

            //timer_DelayBeforeAutomaticMachineVersionCheck.Enabled = true;
            //timer_DelayBeforeAutomaticMachineVersionCheck.Start();
        }


        string folderFullPath;
        string gumGulFolderFullPath = "C:\\Tester Datalog\\GUM";

        private void openLot_Button_Click(object sender, EventArgs e)
        {
            string returnString = "";
            comboBoxToken.Text = "";
            string token = comboBoxToken.Text;
            string lotId = comboBoxLotId.Text;

#if DEBUG
            //string exeFullPathName = Application.ExecutablePath;
            string exeFullPathName = "C:\\VersionControlHubCSharpSample-32bit\\VersionControlHubCSharpSample.exe";
            string repoPath = System.IO.Path.GetDirectoryName(exeFullPathName);// + "\\";

            VersionControl.NET.Dll.CheckProgramVersion(//_FailingThat_ResetBeforeRestart(
                exeFullPathName,
                repoPath,
                ref returnString,
                ref token,
                lotId);
#else
            string exeFullPathName = Application.ExecutablePath;
            //string exeFullPathName = "C:\\VersionControlHubCSharpSample-32bit\\VersionControlHubCSharpSample.exe";
            string repoPath = System.IO.Path.GetDirectoryName(exeFullPathName);// + "\\";

            VersionControl.NET.Dll.CheckProgramVersion(
                exeFullPathName,
                repoPath,
                ref returnString,
                ref token,
                lotId);
#endif

            comboBoxToken.Text = token;

            //if (timer_DelayBeforeAutomaticMachineVersionCheck.Enabled)
            int i = 0;
            string delay = comboBoxDelay.Text;
            bool isNumeric = int.TryParse(delay, out i);

            if (comboBoxDelay.Text != "0" && comboBoxDelay.Text != "" && isNumeric)
            {
                //timer_DelayBeforeAutomaticMachineVersionCheck = new System.Windows.Forms.Timer();
                //timer_DelayBeforeAutomaticMachineVersionCheck.Tick += new EventHandler(timer_DelayBeforeAutomaticMachineVersionCheck_tick);
                timer_DelayBeforeAutomaticMachineVersionCheck.Interval = Convert.ToInt32(comboBoxDelay.Text);
                timer_DelayBeforeAutomaticMachineVersionCheck.Enabled = true;
                timer_DelayBeforeAutomaticMachineVersionCheck.Start();
            }
        }

        //private void openLot_Button_Click(object sender, EventArgs e)
        //{ 
        //MessageBox.Show(GetHostVersionControlStatus().ToString());

        ////Li Jen:  these lines here are not important, just for demonstrating how to call the DLL function.
        //Random rnd = new Random();
        //lotId = "Production_Lot-" + rnd.Next(1000, 9999).ToString();


        ////Li Jen:  From here till end of this function, would be something you need to insert into your code.
        ////string exePath = System.IO.Path.GetDirectoryName(Program.exeFullPathName);
        ////string repoPath = Directory.GetParent(exePath).FullName;
        //string repoPath = System.IO.Path.GetDirectoryName(Program.exeFullPathName);// + "\\";
        //string returnString = "";
        //bool displayBlockingPromptBeforeReset = true;

        ////Li Jen: CheckProgramVersion_FailingThat_RestartWithOutReset is the alternative function to this one if you prefer NOT to reset here but just to restart and let the CheckProgramVersion_FailingThat_ResetBeforeRestart function call at Program Startup to do the job of resetting to latest version.  However this will incur cost of one extra restart.
        //Dll.CheckProgramVersion_FailingThat_ResetBeforeRestart(
        //    Program.exeFullPathName,
        //    repoPath.ToString(),
        //    ref returnString,
        //    displayBlockingPromptBeforeReset,
        //    lotId);

        ////Li Jen: If the CheckProgramVersion_FailingThat_ResetBeforeRestart is supposed to reset and restart
        ////  when it checked and found version is not latest, this Sleep call here
        ////      MAY Be Necessary
        ////  to provide some delay for above DLL function to successfully complete its tasks of resetting to latest 
        ////  version and restarting the program, instead of program execution proceeding straight away to below section.
        //Thread.Sleep(100);

        //folderFullPath = "C:\\Tester Datalog\\Reports\\" + lotId;

        ////Dll.SnapshotOutputFolder_RenameFolderIfExistsAndNotEmpty(
        //Dll.SnapshotOutputFolderWhenOpenLot(
        //    folderFullPath,
        //    lotId);

        //}

        private void closeLot_Button_Click(object sender, EventArgs e)
        {
            ////Li Jen: call at Close Lot
            //Dll.SnapshotOutputFolderWhenCloseLot(
            //    folderFullPath,
            //    lotId);
        }



        private void guOpenLot_Button_Click(object sender, EventArgs e)
        {
            ////Li Jen:  these lines here are not important, just for demonstrating how to call the DLL function.
            //Random rnd = new Random();
            //lotId = "GUML_Lot-" + rnd.Next(1000, 9999).ToString();


            ////Li Jen:  From here till end of this function, would be something you need to insert into your code.
            //string exePath = System.IO.Path.GetDirectoryName(Program.exeFullPathName);
            //string repoPath = Directory.GetParent(exePath).FullName;
            //string returnString = "";
            //bool displayBlockingPromptBeforeReset = true;

            ////Li Jen: CheckProgramVersion_FailingThat_RestartWithOutReset is the alternative function to this one if you prefer NOT to reset here but just to restart and let the CheckProgramVersion_FailingThat_ResetBeforeRestart function call at Program Startup to do the job of resetting to latest version.  However this will incur cost of one extra restart.
            //Dll.CheckProgramVersion_FailingThat_ResetBeforeRestart(
            //    Program.exeFullPathName,
            //    repoPath.ToString(),
            //    ref returnString,
            //    displayBlockingPromptBeforeReset,
            //    lotId);

            ////Li Jen: If the CheckProgramVersion_FailingThat_ResetBeforeRestart is supposed to reset and restart
            ////  when it checked and found version is not latest, this Sleep call here
            ////      MAY Be Necessary
            ////  to provide some delay for above DLL function to successfully complete its tasks of resetting to latest 
            ////  version and restarting the program, instead of program execution proceeding straight away to below section.
            //Thread.Sleep(100);

            //folderFullPath = "C:\\Tester Datalog\\Reports\\" + lotId;

            ////Dll.SnapshotOutputFolder_RenameFolderIfExistsAndNotEmpty(
            //Dll.SnapshotOutputFolderWhenOpenLot(
            //    folderFullPath,
            //    lotId);

            ////Dll.SnapshotOutputFolder_RenameFolderIfExistsAndNotEmpty(
            //Dll.SnapshotOutputFolderWhenOpenLot(
            //    gumGulFolderFullPath,
            //    lotId);

            ////Random rnd = new Random();
            ////lotId = "GULot-" + rnd.Next(1000, 9999).ToString();

            ////string exePath = System.IO.Path.GetDirectoryName(Program.exeFullPathName);
            ////string repoPath = Directory.GetParent(exePath).FullName;
            ////string returnString = "";

            ////Dll.CheckProgramVersion_FailingThat_RestartWithOutReset(
            ////    Program.exeFullPathName,
            ////    repoPath.ToString(),
            ////    ref returnString,
            ////    lotId);


            ////folderFullPath = "Z:\\Tester Datalog\\GU";
            ////bool createFolderIfNonExistent = true;
            ////bool moveExistingContentsToNeighbouringFolder = false;

            ////Dll.SnapshotOutputFolderWhenOpenLot(
            ////    folderFullPath,
            ////    //Dll.EventToSnapshotFolder.OpenGULot,
            ////    lotId);
        }

        private void guCloseLot_Button_Click(object sender, EventArgs e)
        {
            ////Li Jen: call at Close Lot
            //Dll.SnapshotOutputFolderWhenCloseLot(
            //    folderFullPath,
            //    lotId);

            ////Dll.SnapshotOutputFolder_RenameFolderIfExistsAndNotEmpty(
            //Dll.SnapshotOutputFolderWhenCloseLot(
            //    gumGulFolderFullPath,
            //    lotId);
        }


        private static System.Windows.Forms.Timer timer_DelayBeforeAutomaticMachineVersionCheck;

        private void comboBoxDelay_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxDelay.Text == "0" || comboBoxDelay.Text == "")
            {
                buttonMachineVersionStatusManualCheck.Enabled = true;

                //timer_DelayBeforeAutomaticMachineVersionCheck.Enabled = false;
            }
            else
            {
                buttonMachineVersionStatusManualCheck.Enabled = false;

                //timer_DelayBeforeAutomaticMachineVersionCheck.Enabled = true;
            }
        }

        private void buttonMachineVersionStatusManualCheck_Click(object sender, EventArgs e)
        {
            //string machineId = Dll.GetDllKeyValue("CheckProgramVersion", "machineId");

            //bool machineVersionCheckPass =
            //Dll.FailedVersionCheckInfo[] failedVersionCheckInfos =
            Dll.MachineOpenLotVersionCheckInfo machineOpenLotFailedVersionCheckInfo =
                Dll.CheckMachineVersion(
                    //machineId,//comboBoxTestMachineId.Text,
                    comboBoxLotId.Text,
                    comboBoxToken.Text);// token);

            //if (machineVersionCheckPass)
            //if (failedVersionCheckInfos == null)
            if (machineOpenLotFailedVersionCheckInfo.machineProceedWithOpenLot)
                MessageBox.Show("Machine version check PASS");
            else
            {
                string failureDetails = "";

                for (int i = 0; i < machineOpenLotFailedVersionCheckInfo.programFailedVersionCheckInfos.Length; i++)
                {
                    failureDetails +=
                        machineOpenLotFailedVersionCheckInfo.programFailedVersionCheckInfos[i].ipAddress
                        + ": " + machineOpenLotFailedVersionCheckInfo.programFailedVersionCheckInfos[i].programType
                        + ": " + machineOpenLotFailedVersionCheckInfo.programFailedVersionCheckInfos[i].failureInfo
                        + "\n\n";
                }

                MessageBox.Show(string.Format(
                    "Machine version check FAILED:\n\n{0}",
                    failureDetails));
            }
        }

        private void comboBoxLotId_MouseClick(object sender, MouseEventArgs e)
        {
            int i = 0;
            string s = comboBoxLotId.Text;
            bool result = int.TryParse(s, out i);

            if (result)
            {
                comboBoxLotId.Text = Convert.ToString(Convert.ToInt32(i) + 1);
            }
        }

        private void comboBoxLotId_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void buttonTest_Click(object sender, EventArgs e)
        {

            //string machineId = Dll.GetDllKeyValue(
            //    "CheckProgramVersion", "machineId", "amsVersionControl.NET461");

            //MessageBox.Show(machineId);

            string localIPAddress = Dll.GetLocalIPAddress();

            MessageBox.Show(localIPAddress);
        }

        private void comboBoxToken_SelectedIndexChanged(object sender, EventArgs e)
        {

        }







        //private void button1_Click(object sender, EventArgs e)
        //{
        //    string exeFullPathName = Application.ExecutablePath;// System.Reflection.Assembly.GetEntryAssembly().Location;
        //    string exePath = System.IO.Path.GetDirectoryName(exeFullPathName);
        //    string repoPath = Directory.GetParent(exePath).FullName;

        //    DllInternal.ExitAndSelfDeleteProgramFolder(3, repoPath);
        //}
    }
}
