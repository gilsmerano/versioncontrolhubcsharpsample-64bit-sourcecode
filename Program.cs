﻿using System;
using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using VersionControl.NET;

namespace GILSTesterFFTestStationSample
{

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            string returnString = "";
            string token = null;
            string lotId = null;

#if DEBUG
            //string exeFullPathName = Application.ExecutablePath;
            string exeFullPathName = "C:\\VersionControlHubCSharpSample-32bit\\VersionControlHubCSharpSample.exe";
            string repoPath = System.IO.Path.GetDirectoryName(exeFullPathName);// + "\\";

            VersionControl.NET.Dll.CheckProgramVersion(//_FailingThat_ResetBeforeRestart(
                exeFullPathName,
                repoPath,
                ref returnString,
                ref token,
                lotId);
#else
            string exeFullPathName = Application.ExecutablePath;
            //string exeFullPathName = "C:\\VersionControlHubCSharpSample-32bit\\VersionControlHubCSharpSample.exe";
            string repoPath = System.IO.Path.GetDirectoryName(exeFullPathName);// + "\\";

            VersionControl.NET.Dll.CheckProgramVersion(
                exeFullPathName,
                repoPath,
                ref returnString,
                ref token,
                lotId);
#endif

            Application.Run(new Form1());
        }

        //private static string appGuid = "FAE04EC0-301F-11D3-BF4B-00C04F79EFB*";
        //internal static string exeFullPathName;// = "C:\\GILSTesterFFTestStationSample\\Release\\GILSTesterFFTestStationSample.exe";
    }
}
